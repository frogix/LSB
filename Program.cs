using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace LSB
{
	class Program
	{
		static void Main()
		{
			HideData (
				inputImg: "another.bmp",
				outputImg: "newHidden.bmp",
				secretText: "Russian characters are not supported yet :("
			);

			Console.WriteLine(ExtractData("newHidden.bmp"));
			// CreateImg();
			// byte[] bytes = ReadImg("another.bmp");
			// PrintImgBytes(bytes);
			// ReadAndPrintHidden();
			// ReadFile();
		}

		static void HideData (string inputImg, string outputImg, string secretText)
		{
			byte[] bytes = ReadImg(inputImg);
			HideTextInBytes(secretText, bytes);
			var result = ImgFromByteArray(bytes);
			result.Save(outputImg);
		}

		static string ExtractData (string filename)
		{
			const byte STOP_BYTE = 63;
			int stopCounter = 0;
			string result = "";
			List<byte> secretBytes = new List<byte>();

			byte[] bytes = ReadImg(filename);

			for (int i = 0; i < bytes.Length / 4; i++)
			{
				byte secretByte = 0;
				for (int j = 0; j < 8; j += 2)
				{
					byte chunk = bytes[i * 4 + j / 2].ShiftLeft(6).ShiftRight(j);
					// byte currByte = bytes[i * 4 + j / 2];
					// Console.WriteLine($"Current byte: {currByte}, chunk {ToBin(chunk, 8)}");
					secretByte += chunk;
				}
				secretBytes.Add(secretByte);

				if (secretByte == STOP_BYTE)
					stopCounter++;
				else
					stopCounter = 0;
				if (stopCounter == 20)
				{
					for (int k = 0; k < secretBytes.Count - 20; k++)
						result += (char)secretBytes[k];
					break;
				}

			}
			return result;
		}

		static void ReadAndPrintHidden ()
		{
			byte[] bytes = ReadImg("hidden.bmp");
			PrintImgBytes(bytes);
		}

		static Bitmap ImgFromByteArray (byte[] data)
		{
			var b = new Bitmap("another.bmp");

			var BoundsRect = new Rectangle(0, 0, b.Width, b.Height);
			BitmapData bmpData = b.LockBits(BoundsRect,
				ImageLockMode.WriteOnly,
				b.PixelFormat);

			IntPtr ptr = bmpData.Scan0;

			int bytes = bmpData.Stride * b.Height;

			Marshal.Copy(data, 0, ptr, bytes);
			b.UnlockBits(bmpData);
			return b;
		}

		static void HideTextInBytes (string data, byte[] container)
		{
			byte[] dataBytes = Encoding.ASCII.GetBytes(data + "\u2b7f".Repeat(20));
			// for (int i = 0; i < dataBytes.Length; i++)
			// 	Console.Write("" + dataBytes[i] + " ");
			// Console.WriteLine();

			// Каждый байт данных содержится в четырёх байтах контейнера
			if (container.Length < dataBytes.Length * 4)
			{
				Console.WriteLine("Размер сообщения слишком велик для указанного изображения");
				return;
			}
			for (int i = 0; i < dataBytes.Length; i++)
			{
				// Console.WriteLine("Container: " + ToBin(container[4 * i], 8) + " " + ToBin(container[4 * i+1], 8) + " " + ToBin(container[4 * i+2], 8) + " " + ToBin(container[4 * i+3], 8));
				// Console.WriteLine($"Data: {ToBin(dataBytes[i], 8)}");
				for (int j = 0; j < 8; j += 2)
				{
					byte chunk = dataBytes[i].ShiftLeft(j).ShiftRight(6);
					// Console.Write("Chunk " + ToBin(chunk, 2));
					// Console.WriteLine(" is hidden: " + ToBin(HideChunk(container[4 * i], chunk), 8));
					// Console.WriteLine($"Old byte: {container[4 * i + j / 2]}, new byte: {HideChunk(container[4 * i + j / 2], chunk)}");
					container[4 * i + j / 2] = HideChunk(container[4 * i + j / 2], chunk);
				}
			}
		}

		static byte HideChunk (byte container, byte chunk)
		{
			// Зануляем 2 последних бита контейнера и прибавляем к нему 2 бита данных
			return (byte)(container.ShiftRight(2).ShiftLeft(2) + chunk);
		}

		static string ToBin(int value, int len)
		{
			return (len > 1 ? ToBin(value >> 1, len - 1) : null) + "01"[value & 1];
		}

		static void ReadFile()
		{
			byte[] secretBytes = File.ReadAllBytes("secret.txt");
			Console.WriteLine(secretBytes);
			for (int i = 0; i < secretBytes.Length; i++)
			{
				Console.Write(secretBytes[i].ToString() + " ");
			}

		}

		static void CreateImg ()
		{
			Bitmap img = new Bitmap(256, 256);
			int color;

			for (int y = 0; y < 256; y++)
			for (int x = 0; x < 256; x++)
			{
				color = (x+y) % 32 > 16 ? 0 : 255;
				img.SetPixel(x, y, Color.FromArgb(255,color,color,color));
			}
			img.Save("another.bmp");
		}

		static byte[] ReadImg (string filename)
		{
			Bitmap img = new Bitmap(filename);
			return Util.GetRGBValues(img);
		}

		static void PrintImgBytes (byte[] bytes)
		{
			Console.WriteLine(bytes.Length / 4);
			for (int i = 0; i < 100; i += 4)
			{
				Console.WriteLine(
					Align(bytes[i].ToString("G4"), 5) +
					Align(bytes[i+1].ToString("G4"), 5) +
					Align(bytes[i+2].ToString("G4"), 5) +
					Align(bytes[i+3].ToString("G4"), 5)
					);
			}
		}

		static string Align (string s, int space)
		{
			int availSpace = (space - s.Length) / 2;
			s = s.PadLeft(space - availSpace);
			if (s.Length % 2 != 0)
			s = s.PadRight(space);
			else
			s = s.PadRight(space);
			return s;
		}
	}

	public static class Util
	{
		public static string Repeat (this string s, int n)
		{
			return "".PadLeft(n, 'X').Replace("X", s);
		}

		public static byte ShiftLeft (this byte b, int n)
		{
			return (byte)(b << n);
		}

		public static byte ShiftRight (this byte b, int n)
		{
			return (byte)(b >> n);
		}

		public static byte[] GetRGBValues (Bitmap bmp)
		{
			// Lock the bitmap's bits.
			Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
			BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadOnly, bmp.PixelFormat);

			// Get the address of the first line.
			IntPtr ptr = bmpData.Scan0;

			// Declare an array to hold the bytes of the bitmap.
			int bytes = bmpData.Stride * bmp.Height;
			byte[] rgbValues = new byte[bytes];

			// Copy the RGB values into the array.
			System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
			bmp.UnlockBits(bmpData);

			return rgbValues;
		}

	}
}
